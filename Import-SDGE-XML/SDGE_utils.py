import os
import datetime
import zipfile
import time
import csv
import json
import gzip
import http.client
import shutil
import logging
from datetime import date
from os.path import dirname, abspath

def get_date(filenames):
    date = []
    for name in filenames:
        date.append(name[:8])
    return date

def remove_DS_Store(filenames):
    if os.path.isfile('.DS_Store'):
        filenames.remove('.DS_Store')

def remove_noninterval_files(files):
    for file in files:
        if file[14:22] != "Interval":
            files.remove(file)

def convert_to_epoch(date_time): 
    #converts time stamp from SCE to epoch millisecond for GS
    os.environ['TZ']='UTC'
    pattern = '%Y-%m-%d %H:%M:%S'
    epoch = int(time.mktime(time.strptime(date_time, pattern)))
    epoch = 1000 * epoch
    return epoch

def get_time():
    now = datetime.datetime.now()
    return now.strftime("%m%d%Y %H%M")

def remove_folder(path):
    # check if folder exists
    if os.path.exists(path):
         # remove if exists
         shutil.rmtree(path)

def InitLogAndMap(main_folder, uname): 
    # create logging file
    # check if folder exists
    log_folder = os.path.join(main_folder, "LogHistory")
    if not os.path.exists(log_folder):
        os.makedirs(log_folder)
    today = date.today()
    log_file = os.path.join(log_folder, uname+'_GS-LOG_'+today.isoformat()+'.txt')

    logging.basicConfig(filename=log_file,level=logging.INFO,
                    format='%(asctime)s - %(message)s',
                    datefmt='%m/%d/%Y %I:%M:%S %p')
    logging.info('=' * 60)
    #find ID-Map: ..\GroveStreams\Program Status Update.csv'
    GS_root = dirname(dirname(abspath(__file__)))
    IDMapCSV = os.path.join(GS_root, 'Program Status Update.csv')

    return IDMapCSV

def AddLog(msg):
    print(msg)
    logging.info(msg)

def initialize(main_folder, uname): 
    #deletes the contents of the Raw folder
    data_folder = os.path.join(main_folder, "Data")
    rawdata = os.path.join(data_folder, "RawData")

    for folder in os.listdir(rawdata):
        folder_path = os.path.join(rawdata,folder)
        try:
            remove_folder(folder_path)
        except Exception as e:
            print(e)

    return

# =========================================================================
#unzips all .zip files in the "Unprocessed" folder and places the unzipped files in the "RawData" folder
#and puts the .zip files in thr processed folder w/ a "Processed" and time stamp
def unzip(data_folder):
    sys_time = get_time()
    
    path_to_zip_files = os.path.join(data_folder, "Unprocessed")
    path_to_processed_zip_files = os.path.join(data_folder, "Processed")
    directory_to_extract_to = os.path.join(data_folder, "RawData")
    
    current_folder = os.getcwd()
    os.chdir(path_to_zip_files)
    zipfile_names = os.listdir(path_to_zip_files)
    
    for name in zipfile_names:
        zip_name = os.path.join(path_to_zip_files, name)
        processed_zip_name = os.path.join(path_to_processed_zip_files, name[:-4] + " Processed " + sys_time + ".zip")
        raw_data_folder = os.path.join(directory_to_extract_to, name)
        if not os.path.exists(raw_data_folder):
            AddLog("Unzip to: " + raw_data_folder)
            os.makedirs(raw_data_folder)
            zip_ref = zipfile.ZipFile(zip_name, 'r')
            zip_ref.extractall(raw_data_folder)
            zip_ref.close()
            
        os.rename(zip_name, processed_zip_name)

    os.chdir(current_folder)

def put(feed,api_key,templateID):
    
    url = '/api/feed?compTmplId=' + templateID
    headers = {"Content-Encoding" : "gzip" , "Connection" : "close",
            "Content-type" : "application/json", "Cookie" : "api_key="+api_key}                          

    try:
        json_encoded = json.dumps(feed);
       
        #Compress the JSON HTTP body
        body = gzip.compress(json_encoded.encode('utf-8'))

        #Upload the feed to GroveStreams
        conn = http.client.HTTPConnection('www.grovestreams.com')
        conn.request("PUT", url, body, headers)

        #Check for errors
        response = conn.getresponse()
        status = response.status

        AddLog('HTTP Upload: ' + response.reason)
        #AddLog('HTTP Status: %d' % (status))

    except Exception as e:
        #Review log for actions (manual)
        AddLog('Failure: ' + str(e))
       
    finally:      
        #Pause for max ten seconds due to GS IMPORT_RATE_LIMIT
        if templateID.startswith('PGE'):
            time.sleep(10)    # increase for PUT's if in loops

    if conn != None:
       conn.close()

#take in the path to the GS unzipped .csv file (-Billing/-Prgm_Prtcpn.csv) and main directory
#append to Aggregated-Billing.csv and Aggregated-Prgm_Prtcpn.csv accordingly
def AppendCSV(infile,main_folder,filetype):

    outfile = os.path.join(main_folder, "Aggregated"+filetype)  #assume existing without checking existencies
        
    with open(infile, "r", newline='') as inputfile, open(outfile, "a",newline='') as outputfile:
        reader = csv.reader(inputfile, delimiter = '|')
        ##next(reader, None)  # skip the headers - NO HEADERS in SDG&E DRAMSTEM_SUBSCRIPTION
        writer = csv.writer(outputfile, delimiter = '|')
                
        for row in reader:
            writer.writerow(row)
                        
    inputfile.close()
    outputfile.close()
    
    return 

# Move non-Interval.csv to _OTHERS folder
def ExtractToOthers(infile,main_folder):
    other_folder = os.path.join(main_folder, "_OTHERS")
    outfile = os.path.join(other_folder, infile)
    if os.path.exists(outfile):
        os.remove(outfile)
    os.rename(infile, outfile)
    
    return

# Load 'Program Status Update.csv'  to array
def LoadIDMap(IDMapCSV, utility):
    SysIDs = []    # use SFDC's System Record ID
        
    with open(IDMapCSV, "r",newline='') as outputfile:
        reader = csv.reader(outputfile, delimiter = ',')
        next(reader, None)  # skip the headers
        # define columns from csv
        col_SysID = 3   
        col_Utility = 1
        col_UUID = 4

        for row in reader:
            uname = row[col_Utility]
            uname = uname.replace('&','')
            if (uname == utility):
                SysIDs.append([row[col_SysID],row[col_UUID]])
                        
    outputfile.close()    
    return SysIDs

# Pass in UUID from XML, map to SFDCID as Component name/ID
def GetCompID(UUID, IDMap):
    # IDMap columns = [SysID, UUID]
    for i in range(len(IDMap)):
        if (IDMap[i][1] == UUID):
            if (IDMap[i][0] != ''):
                return IDMap[i][0]   # found matching SysID

    return '--'+UUID    # use UUID as-is
